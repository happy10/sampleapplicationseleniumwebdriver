package com.smtest.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.smtest.data.AppUser;
import com.smtest.data.UserData;
import com.smtest.fw.HomePage;
import com.smtest.fw.SignInPage;
import com.smtest.fw.SignUpPage;
import com.smtest.fw.UsersPage;

public class TestSignIn {

	WebDriver wd;
	@Test(dataProvider="existingUser", dataProviderClass=UserData.class)
	public void testSignInExistingUser(AppUser user){
		SignInPage sighInPage = new SignInPage(wd);
		UsersPage usersPage = sighInPage
				.existingUserSignin(user);
		Assert.assertTrue(usersPage.usersName().equals(user.userName));
		usersPage.signOut();
	}
	
	@Test(dataProvider="newUser", dataProviderClass=UserData.class)
	public void testInvalidSigninErr(AppUser user){
		SignInPage sighInPage = new SignInPage(wd);
		Assert.assertTrue(sighInPage.invalidSignIn(user).equals("Invalid email/password combination"));
	}
	
	@Test(dataProvider="newUser", dataProviderClass=UserData.class)
	public void testVisitorSignUp(AppUser user){
		HomePage homePage = new HomePage(wd);
		SignUpPage siteVisitor = homePage.signUp();
		UsersPage newSignedUpUser = siteVisitor.createAccount(user);
		Assert.assertTrue(newSignedUpUser.welcome().equals("Welcome to the Sample App"));
		Assert.assertTrue(newSignedUpUser.usersName().equals(user.getUserName()));
		newSignedUpUser.signOut();
	}
	@BeforeClass
	public void beforeClass() {
	  System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver.exe");
		wd = new ChromeDriver();
		wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	@AfterClass
	public void afterClass() {
		wd.quit();
	}
}