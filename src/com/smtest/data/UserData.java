package com.smtest.data;

import java.util.Random;

import org.testng.annotations.DataProvider;

public class UserData {
	static Random rnd = new Random();
	
    @DataProvider(name="newUser")
    public static Object[][] newUsers(){
    	String randName = "tester"+rnd.nextInt(11111);
    	return new Object[][]{
    			{new AppUser()
				.setName(randName)
				.setEmail(randName+"nonexistantzzz@aa.com")
				.setPassword("hahahawillnotwork)}
    	};
    }
    
    @DataProvider(name="existingUser")
    public static Object[][] existingUsers(){
    	return new Object[][]{
    			{new AppUser()
				.setName("tester1")
				.setEmail("sometester1@notinthelist.com")
				.setPassword("hahahawillnotwor")}
    	};
    }
}