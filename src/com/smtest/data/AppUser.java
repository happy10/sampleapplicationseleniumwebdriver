package com.smtest.data;

public class AppUser {
	public String email;
	public String password;
	public String userName;
	
	public AppUser(String email, String password, String userName){
		this.email=email;
		this.password=password;
		this.userName=userName;
	}

	public AppUser() {
		// TODO Auto-generated constructor stub
	}

	public String getPassword() {
		return password;
	}
	public String getUserName() {
		return userName;
	}
	public String getEmail() {
		return email;
	}
	public AppUser setName(String userName){
		this.userName = userName;
		return this;
	}
	public AppUser setEmail(String email){
		this.email = email;
		return this;
	}
	public AppUser setPassword(String password){
		this.password = password;
		return this;
	}
	@Override
    public String toString(){
    	return String.format("user: %s, email: %s",this.userName,this.email);
    }
}
