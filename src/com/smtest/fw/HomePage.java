package com.smtest.fw;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends PageBase{

	private String url;

	public HomePage(WebDriver wd) {
		super(wd);
		this.url = baseUrl;
		wd.get(url);
	}
	
	public SignUpPage signUp() {
		clickWhenReady(By.cssSelector(".btn.btn-large.btn-privary"));
		return new SignUpPage(wd);
	}
}
