package com.smtest.fw;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smtest.data.AppUser;

public class SignUpPage extends PageBase{

	private String url;

	public SignUpPage(WebDriver wd) {
		super(wd);
		this.url = baseUrl + "signup";
		wd.get(url);
	}
	public UsersPage createAccount(AppUser user){
		fillForm(user);
		submit();
		return new UsersPage(wd, user);
	}
	private void fillForm(AppUser user){
		typeWhenReady(user.getUserName(), By.cssSelector("#user_name"));
		typeWhenReady(user.getEmail(), By.cssSelector("#user_email"));
		typeWhenReady(user.getPassword(), By.cssSelector("#user_password"));
		typeWhenReady(user.getPassword(), By.cssSelector("#user_password_confirmation"));
	}
	private void submit(){
		clickWhenReady(By.cssSelector(".btn.btn-large.btn-primary"));
	}
}
