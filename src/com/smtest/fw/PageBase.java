package com.smtest.fw;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {
	WebDriver wd;
	WebDriverWait wdwait;
	String baseUrl;
	String resource;
	public PageBase(WebDriver wd){
		this.wd=wd;
		this.wdwait = new WebDriverWait(wd, 10);
		this.baseUrl = "https://smtest-sampleapp.herokuapp.com/";
	}
	
	public String getTitle(){
		return wd.getTitle();
	}

	protected void click(By locator) {
		wd.findElement(locator).click();
	}
	protected void clickWhenReady(By locator) {
		WebElement element = wdwait.until(ExpectedConditions.elementToBeClickable(locator));
		element.click();
	}

	protected void type(String address, By locator) {
		WebElement element = wd.findElement(locator);
		element.clear();
		element.sendKeys(address);
	}
	protected void typeWhenReady(String text, By locator) {
		WebElement element = wdwait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.clear();
		element.sendKeys(text);
	}
	protected WebElement element(By locator){
		WebElement element = wdwait.until(ExpectedConditions.presenceOfElementLocated(locator));
		return element;
	}

	public void signOut() {
		clickWhenReady(By.cssSelector(".dropdown-toggle"));
		clickWhenReady(By.xpath(".//*[@id='fat-menu']/ul/li[4]/a"));
	}


}
