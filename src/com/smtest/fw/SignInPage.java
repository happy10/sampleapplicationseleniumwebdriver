package com.smtest.fw;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smtest.data.AppUser;

public class SignInPage extends PageBase{

	private String url;

	public SignInPage(WebDriver wd) {
		super(wd);
		this.url = baseUrl + "signin";
		wd.get(url);
	}
	
	public UsersPage existingUserSignin(AppUser user){
		fillForm(user);
    	submit();
    	return new UsersPage(wd, user);
	}
	public String invalidSignIn(AppUser user){
		fillForm(user);
		submit();
		return element(By.cssSelector(".alert.alert-error")).getText();
	}
	private void fillForm(AppUser user){
		typeWhenReady(user.getEmail(), By.id("session_email"));
		typeWhenReady(user.getPassword(), By.id("session_password"));
	}
	private void submit(){
		clickWhenReady(By.name("commit"));
	}
	public void signOut() {
		wd.findElement(By.cssSelector(".dropdown-toggle")).click();
		wd.findElement(By.cssSelector(".dropdown-menu>li>a")).click();
	}
}
