package com.smtest.fw;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smtest.data.AppUser;

public class UsersPage extends PageBase{
	public UsersPage(WebDriver wd, AppUser user) {
		super(wd);
		this.resource = "users";
		assert getTitle().contains(user.getUserName());
	}	
	
	public String usersName(){
		return element(By.cssSelector(".span4>section>h1")).getText();
	}
	public String welcome(){
		return element(By.cssSelector(".alert.alert-success")).getText();
	}
}
